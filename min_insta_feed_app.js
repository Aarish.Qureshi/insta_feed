function insertInstaFeedJs(src, callback) {
    var script = document.createElement("script");
    script.type = "text/javascript";

    script.src = src;
    if (typeof callback == "function") {
        script.addEventListener("load", callback);
    }

    document.getElementsByTagName("head")[0].appendChild(script);
}

insertInstaFeedJs("https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js", function() {
    window.$jqObj = jQuery.noConflict(true);
    let feedSectionObj = $jqObj("#mn-ifd-sel-layout")
    let config_id = feedSectionObj.attr('config_id')
    console.log(config_id)
    
    if (feedSectionObj.length) {
        
        function fetchInstaFeedConfig() {
            let main_url = "https://photograph-andy-foreign-soc.trycloudflare.com"
    
            $jqObj.ajax({
                url: `${main_url}insta_feed_api/scriptag-feed-config/`,
                type: "GET",
                dataType: "jsonp",
                jsonpCallback: "instaFeedConfig",
                crossDomain: true,
                data: {
                    "shop_url": Shopify.shop
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log("Status: " + textStatus);
                    console.log("Error: " + errorThrown);
                }
            });
        }
    
        function setFeedLayout(lytType, lytHgltCategory, postsHtml, lytPostsCount, hgltImgHtml) {
            if (lytType == "cfg-grid") {
                feedSectionObj.append(`
                    <div class="row g-0 row-cols-xl-${lytPostsCount} row-cols-lg-${lytPostsCount - 1} row-cols-md-${lytPostsCount - 2} row-cols-sm-${lytPostsCount - 3} row-cols-1">
                        ${postsHtml}
                    </div>
                `)
            }
    
            else if (lytType == "cfg-slider") {
                feedSectionObj.append(`
                    <div class="ifd-slider">
                        ${postsHtml}
                    </div>
                `)
            }
    
            else if (lytType == "cfg-highlight") {
                feedSectionObj.append(`
                    <div class="row">
                        <div role="button" class="ifd-media-post g-0 ${lytHgltCategory == 'hglt-2x3' ? 'col-lg-5' : 'col-lg-6'}">${hgltImgHtml}</div>
                        <div class="${lytHgltCategory == 'hglt-2x3' ? 'col-lg-7' : 'col-lg-6'}">${postsHtml}</div>
                    </div>
                `)
            }
    
            else {
    
            }
        }
    
        function renderMappedProducts(configData, post_id) {
            // let mappedProdsCtr = $jqObj("#mn-ifd-prods-ctr")
            let mappedProdsObj = $jqObj("#mn-ifd-mapped-prods")
    
            mappedProdsObj.empty()
    
            // Get mapped products details
            vData = {}
    
            configData.filter(config => {
                if (config.im_id == post_id && config.p_data.length > 0) {
                    vData = config.p_data
                }
            })
    
            if (vData.length > 0) {
                // mappedProdsCtr.removeAttr("style")
    
                vData.forEach(vt => {
                    mappedProdsObj.append(`
                        <div class="col-lg-12 carousel_current mt-3 text-center">
                            <div class="prod_item">
                                <div class="prod_wrapper">
                                    <div>
                                        <img src="${vt.image}" alt="" class="h-50 w-50" />
                                    </div>
                                    <div class="prod_body">
                                        <div class="prod_title">${vt.title}</div>
                                        <div class="prod_price">${shopCurrencySymbol} ${vt.price}</div>
                                    </div>
                                    <div class="btn btn-dark mb-3">
                                        <a href="/products/${vt.handle}" target="_blank" class="text-white text-decoration-none">VIEW DETAIL</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `)
                })
            }
            // else {
            //     mappedProdsCtr.css({"display": "none"})
            // }
        }
    
        instaFeedConfig = function (resp) {
            let configData = resp.detail
            let lytConfig = resp.config
            let userName = resp.user_name
            let hgltPostHtml = ""

            if (!lytConfig) {
                return
            }

            if(config_id){
                lytConfig = lytConfig.find(item => item.lyt_id == config_id)
            } else {
                lytConfig = lytConfig[0]
            }
            
            console.log(lytConfig)

            let lytType = lytConfig.lyt
            let lytCategory = lytConfig.lyt_category
            let lytHgltCategory = lytConfig.lyt_hglt
            let lytPostsCount = lytConfig.lyt_posts
            let lytRows = lytConfig.lyt_rows
            let lytSpcVal = lytConfig.lyt_ptspc_val
            let lytSpcUnit = lytConfig.lyt_ptspc_unit
            let lytOnClick = lytConfig.lyt_on_click
    
            let postsHtml = hgltImgHtml = ``
            
            if (lytType == "cfg-grid") {
                configData = configData.slice(0, lytPostsCount * lytRows)
    
                configData.forEach(post => {
                    postsHtml += `
                        <div role="button" class="ifd-media-post col" style="${lytCategory == "cfg-posts" ? "" : "height:400px;"}">
                            <img post-id="${post.im_id}" post-caption="${post.im_caption}" post-created-at="${post.im_created_at}" post-url="${post.im_perm_url}" src="${post.im_url}" alt="" class="h-100 w-100">
                            <span class="instagram-item-icon"><svg width="11" height="13" viewBox="0 0 13 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 5.125V4.25C10 2.33594 8.41406 0.75 6.5 0.75C4.55859 0.75 3 2.33594 3 4.25V5.125H0.375V12.5625C0.375 13.793 1.33203 14.75 2.5625 14.75H10.4375C11.6406 14.75 12.625 13.793 12.625 12.5625V5.125H10ZM4.75 4.25C4.75 3.29297 5.51562 2.5 6.5 2.5C7.45703 2.5 8.25 3.29297 8.25 4.25V5.125H4.75V4.25ZM9.125 7.53125C8.74219 7.53125 8.46875 7.25781 8.46875 6.875C8.46875 6.51953 8.74219 6.21875 9.125 6.21875C9.48047 6.21875 9.78125 6.51953 9.78125 6.875C9.78125 7.25781 9.48047 7.53125 9.125 7.53125ZM3.875 7.53125C3.49219 7.53125 3.21875 7.25781 3.21875 6.875C3.21875 6.51953 3.49219 6.21875 3.875 6.21875C4.23047 6.21875 4.53125 6.51953 4.53125 6.875C4.53125 7.25781 4.23047 7.53125 3.875 7.53125Z" fill="white"></path></svg></span>
                        </div>
                    `
                })
            }
    
            else if (lytType == "cfg-slider") {
                configData = configData.slice(0, lytPostsCount)
    
                configData.forEach(post => {
                    postsHtml += `
                        <div class="grid-container">
                            <div role="button" class="ifd-media-post g-0">
                                <img post-id="${post.im_id}" post-caption="${post.im_caption}" post-created-at="${post.im_created_at}" post-url="${post.im_perm_url}" src="${post.im_url}" alt="" class="w-100" style="height: ${lytCategory == "cfg-posts" ? "100% !important" : "400px"}">
                                <span class="instagram-item-icon"><svg width="11" height="13" viewBox="0 0 13 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 5.125V4.25C10 2.33594 8.41406 0.75 6.5 0.75C4.55859 0.75 3 2.33594 3 4.25V5.125H0.375V12.5625C0.375 13.793 1.33203 14.75 2.5625 14.75H10.4375C11.6406 14.75 12.625 13.793 12.625 12.5625V5.125H10ZM4.75 4.25C4.75 3.29297 5.51562 2.5 6.5 2.5C7.45703 2.5 8.25 3.29297 8.25 4.25V5.125H4.75V4.25ZM9.125 7.53125C8.74219 7.53125 8.46875 7.25781 8.46875 6.875C8.46875 6.51953 8.74219 6.21875 9.125 6.21875C9.48047 6.21875 9.78125 6.51953 9.78125 6.875C9.78125 7.25781 9.48047 7.53125 9.125 7.53125ZM3.875 7.53125C3.49219 7.53125 3.21875 7.25781 3.21875 6.875C3.21875 6.51953 3.49219 6.21875 3.875 6.21875C4.23047 6.21875 4.53125 6.51953 4.53125 6.875C4.53125 7.25781 4.23047 7.53125 3.875 7.53125Z" fill="white"></path></svg></span>
                            </div>
                        </div>
                    `
                })
            }
    
            else if (lytType == "cfg-highlight") {
                let tempHgltPostHtml = ``

                if (lytHgltCategory == "hglt-2x2") {
                    configData = configData.slice(0, 5)
                }
                else if (lytHgltCategory == "hglt-2x3") {
                    configData = configData.slice(0, 7)
                }
                else if (lytHgltCategory == "hglt-3x3") {
                    configData = configData.slice(0, 10)
                }
                else {

                }
    
                configData.forEach((post, index_) => {
                    if (index_ == 0) {
                        hgltImgHtml = `
                            <img post-id="${post.im_id}" post-caption="${post.im_caption}" post-created-at="${post.im_created_at}" post-url="${post.im_perm_url}" src="${post.im_url}" alt="" class="${lytHgltCategory == "hglt-2x3" ? "" : "h-100"} w-100">
                            <span class="instagram-item-icon"><svg width="11" height="13" viewBox="0 0 13 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 5.125V4.25C10 2.33594 8.41406 0.75 6.5 0.75C4.55859 0.75 3 2.33594 3 4.25V5.125H0.375V12.5625C0.375 13.793 1.33203 14.75 2.5625 14.75H10.4375C11.6406 14.75 12.625 13.793 12.625 12.5625V5.125H10ZM4.75 4.25C4.75 3.29297 5.51562 2.5 6.5 2.5C7.45703 2.5 8.25 3.29297 8.25 4.25V5.125H4.75V4.25ZM9.125 7.53125C8.74219 7.53125 8.46875 7.25781 8.46875 6.875C8.46875 6.51953 8.74219 6.21875 9.125 6.21875C9.48047 6.21875 9.78125 6.51953 9.78125 6.875C9.78125 7.25781 9.48047 7.53125 9.125 7.53125ZM3.875 7.53125C3.49219 7.53125 3.21875 7.25781 3.21875 6.875C3.21875 6.51953 3.49219 6.21875 3.875 6.21875C4.23047 6.21875 4.53125 6.51953 4.53125 6.875C4.53125 7.25781 4.23047 7.53125 3.875 7.53125Z" fill="white"></path></svg></span>
                        `
                    }
    
                    else {
                        tempHgltPostHtml += `
                            <div role="button" class="ifd-media-post g-0 ${lytHgltCategory == "hglt-2x3" ? "col-lg-4" : "col"}">
                                <img post-id="${post.im_id}" post-caption="${post.im_caption}" post-created-at="${post.im_created_at}" post-url="${post.im_perm_url}" src="${post.im_url}" alt="" class="h-100 w-100" height=${lytCategory == "cfg-posts" ? "220px" : "380px"} width="220px">
                                <span class="instagram-item-icon"><svg width="11" height="13" viewBox="0 0 13 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 5.125V4.25C10 2.33594 8.41406 0.75 6.5 0.75C4.55859 0.75 3 2.33594 3 4.25V5.125H0.375V12.5625C0.375 13.793 1.33203 14.75 2.5625 14.75H10.4375C11.6406 14.75 12.625 13.793 12.625 12.5625V5.125H10ZM4.75 4.25C4.75 3.29297 5.51562 2.5 6.5 2.5C7.45703 2.5 8.25 3.29297 8.25 4.25V5.125H4.75V4.25ZM9.125 7.53125C8.74219 7.53125 8.46875 7.25781 8.46875 6.875C8.46875 6.51953 8.74219 6.21875 9.125 6.21875C9.48047 6.21875 9.78125 6.51953 9.78125 6.875C9.78125 7.25781 9.48047 7.53125 9.125 7.53125ZM3.875 7.53125C3.49219 7.53125 3.21875 7.25781 3.21875 6.875C3.21875 6.51953 3.49219 6.21875 3.875 6.21875C4.23047 6.21875 4.53125 6.51953 4.53125 6.875C4.53125 7.25781 4.23047 7.53125 3.875 7.53125Z" fill="white"></path></svg></span>
                            </div>
                        `

                        if(index_ == (configData.length - 1)){
                        
                            if (lytHgltCategory == "hglt-2x2") {
                                postsHtml += `
                                    <div class="row row-cols-lg-2 row-cols-md-2 row-cols-sm-2 row-cols-1">
                                        ${tempHgltPostHtml}
                                    </div>
                                `
                            }

                            else if (lytHgltCategory == "hglt-2x3") {
                                postsHtml += `
                                    <div class="row row-lg-2 row-md-2 row-sm-2 row-1">
                                        ${tempHgltPostHtml}
                                    </div>
                                `
                            }

                            else if (lytHgltCategory == "hglt-3x3") {
                                postsHtml += `
                                    <div class="row row-cols-lg-3 row-cols-md-3 row-cols-sm-3 row-cols-1">
                                        ${tempHgltPostHtml}
                                    </div>
                                `
                            }

                            else {

                            }

                        }
                    }
    
                })
            }
    
            else {
    
            }
    
            if (postsHtml != ``) {
                setFeedLayout(lytType, lytHgltCategory, postsHtml, lytPostsCount, hgltImgHtml)
            }
    
            // Set Insta Feed Spacing
            $jqObj(".ifd-media-post").find("img").css({
                "padding-right": `${lytSpcVal}${lytSpcUnit}`,
                "padding-bottom": `${lytSpcVal}${lytSpcUnit}`
            })
    
            $jqObj(".ifd-media-post").click(function(e) {
                let imgObj = $jqObj(this).find("img")
                let img_src = imgObj.attr("src")
                let post_id = imgObj.attr("post-id")
                let post_url = imgObj.attr("post-url")
                let postCaption = imgObj.attr("post-caption")
                let postCreatedAt = imgObj.attr("post-created-at")
    
                let mnIfdPopupObj = $jqObj("#mn-ifd-popup")

                if (lytOnClick == "cfg-ignore") {
    
                }
    
                else if (lytOnClick == "cfg-redirect") {   
                    window.open(post_url, "_blank");
                }
    
                else {
    
                    let posts_list = configData.map(obj => {
                        return obj.im_id
                    })
    
                    mnIfdPopupObj.html("")
                    let popup_body = $jqObj(`
                        <div class="popup-container">
                            <div class="popup-container-body d-flex align-items-center justify-content-center" style="height: 80vh; overflow:hidden;">
                                <div class="row g-0 h-100 w-100" style="overflow-y: auto;">
                                    <div class="col-lg-8 h-100">
                                        <img post-id="${post_id}" src='${img_src}' class="ifd-popup-img" />
                                    </div>
                                
                                    <div id="mn-ifd-popup-right-ctr" class="col-lg-4" style="overflow-y: auto;">
                                        <div class="d-flex align-items-end flex-column row-cols-lg-1 row-cols-md-1 row-cols-sm-1 row-cols-1 feed_info">
                    
                                            <div class="insta_header" data-toggle="tooltip" data-placement="top" title="${userName}">
                                                <a class="acc_info" href="https://instagram.com/${userName}" target="_blank">
                                                    <div class="icon_header">
                                                        <div class="icon_wrapper">
                                                            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http:3.org/2000/svg"><path d="M21.75 6.125C21.75 7.16053 20.9105 8 19.875 8C18.8395 8 18 7.16053 18 6.125C18 5.08947 18.8395 4.25 19.875 4.25C20.9105 4.25 21.75 5.08947 21.75 6.125Z" fill="white"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M13 19.25C16.4518 19.25 19.25 16.4518 19.25 13C19.25 9.54822 16.4518 6.75 13 6.75C9.54822 6.75 6.75 9.54822 6.75 13C6.75 16.4518 9.54822 19.25 13 19.25ZM13 16.75C15.0711 16.75 16.75 15.0711 16.75 13C16.75 10.9289 15.0711 9.25 13 9.25C10.9289 9.25 9.25 10.9289 9.25 13C9.25 15.0711 10.9289 16.75 13 16.75Z" fill="white"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M0.5 12.5C0.5 8.29961 0.5 6.19941 1.31745 4.59507C2.0365 3.18386 3.18386 2.0365 4.59507 1.31745C6.19941 0.5 8.29961 0.5 12.5 0.5H13.5C17.7004 0.5 19.8006 0.5 21.4049 1.31745C22.8161 2.0365 23.9635 3.18386 24.6825 4.59507C25.5 6.19941 25.5 8.29961 25.5 12.5V13.5C25.5 17.7004 25.5 19.8006 24.6825 21.4049C23.9635 22.8161 22.8161 23.9635 21.4049 24.6825C19.8006 25.5 17.7004 25.5 13.5 25.5H12.5C8.29961 25.5 6.19941 25.5 4.59507 24.6825C3.18386 23.9635 2.0365 22.8161 1.31745 21.4049C0.5 19.8006 0.5 17.7004 0.5 13.5V12.5ZM12.5 3H13.5C15.6414 3 17.0972 3.00194 18.2224 3.09388C19.3184 3.18343 19.879 3.34574 20.27 3.54497C21.2108 4.02433 21.9757 4.78924 22.455 5.73005C22.6543 6.12105 22.8166 6.68156 22.9061 7.77759C22.9981 8.90282 23 10.3586 23 12.5V13.5C23 15.6414 22.9981 17.0972 22.9061 18.2224C22.8166 19.3184 22.6543 19.879 22.455 20.27C21.9757 21.2108 21.2108 21.9757 20.27 22.455C19.879 22.6543 19.3184 22.8166 18.2224 22.9061C17.0972 22.9981 15.6414 23 13.5 23H12.5C10.3586 23 8.90282 22.9981 7.77759 22.9061C6.68156 22.8166 6.12105 22.6543 5.73005 22.455C4.78924 21.9757 4.02433 21.2108 3.54497 20.27C3.34574 19.879 3.18343 19.3184 3.09388 18.2224C3.00194 17.0972 3 15.6414 3 13.5V12.5C3 10.3586 3.00194 8.90282 3.09388 7.77759C3.18343 6.68156 3.34574 6.12105 3.54497 5.73005C4.02433 4.78924 4.78924 4.02433 5.73005 3.54497C6.12105 3.34574 6.68156 3.18343 7.77759 3.09388C8.90282 3.00194 10.3586 3 12.5 3Z" fill="white"></path></svg>
                                                        </div>
                                                    </div>
                                                    <div class="insta_username">${userName}</div>
                                                </a>
                                                <div class="spacing"></div>
                                                <div>
                                                    <a class="follow_header" href="https://instagram.com/${userName}" target='_blank'>
                                                        <div style="background: #5B86E5; border-radius: 3px; min-height: 32px; padding: 0 16px;border: none; display: inline-flex; align-items: center"><strong><span style="color:#FFFFFF">Follow</span></strong></div>
                                                    </a>
                                                </div>
                                            </div>
    
                                            <!-- <div id="ifd-popup-caption" class="mt-4 text-center">
                                                ${postCaption}
                                            </div> -->
                                            
                                            <div id="mn-ifd-mapped-prods" class="mt-2 mb-4 px-5"></div>

                                            <hr/>
                                            <div id="ifd-popup-created-at" class="mt-auto">
                                                ${postCreatedAt} <a class="text-black" href="${post_url}" target="_blank">(View On Instagram)</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    
                                <a class="prev"><svg width="12" height="22" viewBox="0 0 12 22" fill="none" xmlns="http:3.org/2000/svg"> <path d="M10.8281 21.2188L11.7656 20.3281C11.9531 20.0938 11.9531 19.7188 11.7656 19.5312L3.28125 11L11.7656 2.51562C11.9531 2.32812 11.9531 1.95312 11.7656 1.71875L10.8281 0.828125C10.5938 0.59375 10.2656 0.59375 10.0312 0.828125L0.1875 10.625C0 10.8594 0 11.1875 0.1875 11.4219L10.0312 21.2188C10.2656 21.4531 10.5938 21.4531 10.8281 21.2188Z" fill="#F3F3F3"></path> </svg></a>
                                <a class="next"><svg width="12" height="22" viewBox="0 0 12 22" fill="none" xmlns="http:3.org/2000/svg"> <path d="M1.125 0.828125L0.1875 1.71875C0 1.95312 0 2.32812 0.1875 2.51562L8.67188 11L0.1875 19.5312C0 19.7188 0 20.0938 0.1875 20.3281L1.125 21.2188C1.35938 21.4531 1.6875 21.4531 1.92188 21.2188L11.7656 11.4219C11.9531 11.1875 11.9531 10.8594 11.7656 10.625L1.92188 0.828125C1.6875 0.59375 1.35938 0.59375 1.125 0.828125Z" fill="#F3F3F3"></path> </svg></a>
                                
                            </div>
                        </div>
                    `)
    
                    mnIfdPopupObj.append(popup_body)
                    mnIfdPopupObj.addClass("is-visible");
                    $jqObj("#ifd-popup-caption").css({
                        "font-size": "13px"
                    })
                    $jqObj("#ifd-popup-created-at").css({
                        "font-size": "13px",
                        "font-weight": 600,
                        "padding-left": "10px",
                        // "background-color": "aliceblue"
                    })
        
                    renderMappedProducts(configData, post_id)
            
                    let indx = posts_list.indexOf(post_id)
            
                    popup_body.find(".prev").click(() => {
                        indx = indx - 1
                        if (indx < 0){
                            indx = posts_list.length - 1
                        }
        
                        prevImgObj = configData.filter(config => {
                            if (config.im_id == posts_list[indx]) {
                                return config
                            }
                        })
        
                        if (prevImgObj.length > 0) {
                            popup_body.find("img").attr("src", prevImgObj[0].im_url)
                            popup_body.find("#ifd-popup-caption").empty().html(prevImgObj[0].im_caption)
                            popup_body.find("#ifd-popup-created-at").empty().html(prevImgObj[0].im_created_at)
                            
                            renderMappedProducts(configData, prevImgObj[0].im_id)
                        }
        
                    })
            
                    popup_body.find(".next").click(() => {
                        indx = indx + 1
                        if (indx == posts_list.length){
                            indx = 0
                        }
        
                        nextImgObj = configData.filter(config => {
                            if (config.im_id == posts_list[indx]) {
                                return config
                            }
                        })
        
                        if (nextImgObj.length > 0) {
                            popup_body.find("img").attr("src", nextImgObj[0].im_url)
                            popup_body.find("#ifd-popup-caption").empty().html(nextImgObj[0].im_caption)
                            popup_body.find("#ifd-popup-created-at").empty().html(nextImgObj[0].im_created_at)
                            
                            renderMappedProducts(configData, nextImgObj[0].im_id)
                        }
        
                    })
                }
            })
    
            $('.ifd-slider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: true,
                infinite:true,
                autoplaySpeed: 2000,
                prevArrow: "<span class='slick-prev'></span>",
                nextArrow: "<span class='slick-next'></span>",
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 678,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 576,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        }
    
        fetchInstaFeedConfig()
    
        $jqObj('.popup-trigger').on('click', function(event){
            event.preventDefault();
            $jqObj('.popup').addClass('is-visible');
        });
        
        $jqObj('.popup').on('click', function(event){
            if( $(event.target).is('.popup-close') || $(event.target).is('.popup') ) {
                event.preventDefault();
                $(this).removeClass('is-visible');
            }
        });

    }

})
